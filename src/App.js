import React from 'react';
import Datos from './components/Datos';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <div className="page-app">
          <Datos />
        </div>
      </header>
    </div>
  );
}

export default App;
