import React, { Component } from 'react';
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';

class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputText: '',
      isNumber: null,
      oraciones: null,
      isntObject: null,
    };
  }

  asignarValor = input => {
    const { target } = input;
    this.setState({ inputText: target.value });
  }

  takeSentences = cadena => {
    const vector = cadena.split(',');
    return this.setState({ isNumber: false, oraciones: vector.length, inputText: "", isntObject: false });
  }

  evaluateString = () => {
    const { inputText } = this.state;
    if (!isNaN(Number(inputText))) {
      return this.setState({ isNumber: true, inputText: "", oraciones: null, isntObject: false });
    }
    this.takeSentences(inputText);
  }

  

  evaluateObject = () => {
    const { inputText } = this.state;
    console.log("here",inputText.substring(0,1),inputText.substring(inputText.length-1))
    if (inputText.substring(0,1) !== '{' || inputText.substring(inputText.length - 1) !== '}') {
      return this.setState({ isntObject: true, inputText: "", isNumber: false, oraciones: false });
    }
    this.takeSentences(inputText);
  }

  render() {
    const { isNumber, inputText, oraciones, isntObject } = this.state;
    return (
      <div className="form-group">
        <input value={inputText} type="text" placeholder="Por favor digite su cadena" onChange={this.asignarValor} />
        <button className="btn-primary" onClick={this.evaluateString} >Evaluar </button>
        <button className="btn-success" onClick={this.evaluateObject} >Evaluar Objeto</button>
        {
          isntObject &&
          <div className="alert alert-danger" role="alert">
            La cadena no es un objeto!
          </div>
        }
        {
          isNumber &&
          <div className="alert alert-danger" role="alert">
            Es un número!
          </div>
        }
        {
          oraciones &&
          <div className="alert alert-success" role="alert">
            La cadena tiene {oraciones} oraciones!
            {
              oraciones >= 5?
              <span> Verdadero</span>:<span> Falso</span>
            }
          </div>
        }
        
      </div>
    );
  }
}

export default index;
